# HW Design UI

## TODO
* Redesign FL Corperate project page to brand guideline

## Duration
* 3-5 days

## Example of corporate project page
![example](https://gitlab.com/pitipon-founderslair/HW-Design-UI/-/raw/main/example-corporate-projects.png)

## Brandguide
https://gitlab.com/pitipon-founderslair/HW-Design-UI/-/raw/main/brandguide.pdf?inline=false